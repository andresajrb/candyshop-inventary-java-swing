/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package formularios;

import clases.Cliente;
import clases.Datos;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author andres
 */
public class frmBusquedaCliente extends javax.swing.JDialog {

    Datos misDatos = new Datos();
    
    private ArrayList<Cliente> listaClientes = new ArrayList<>();
    
    private Cliente clienteSelect = new Cliente();
    
    private Cliente respuesta = new Cliente();
    
    // Tabla de datos
    private DefaultTableModel tablaPrincipal;

    public void setDatos(Datos datos) {
        this.misDatos = datos;
    }
    
    public void llenarTabla() {
        String titulos[] = {"Codigo", "Nombres", "Apellidos", "ID"};
        String registro[] = new String[4];
        tablaPrincipal = new DefaultTableModel(null, titulos);

        for (int i = 0; i < this.listaClientes.size(); i++) {
            registro[0] = Integer.toString(this.listaClientes.get(i).getCodigoCliente());
            registro[1] = this.listaClientes.get(i).getNombre();
            registro[2] = this.listaClientes.get(i).getApellido();
            registro[3] = this.listaClientes.get(i).getId();
            tablaPrincipal.addRow(registro);
        }

        tablaBusquedaCliente.setModel(tablaPrincipal);

    }
    
    public void generarVista(int auxCursor) {
        this.clienteSelect = null;
        this.clienteSelect = listaClientes.get(auxCursor);
    }

    public Cliente getClienteSelect() {
        return clienteSelect;
    }

    public void setClienteSelect(Cliente clienteSelect) {
        this.clienteSelect = clienteSelect;
    }

    public Cliente getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(Cliente respuesta) {
        this.respuesta = respuesta;
    }
    
    

    /**
     * Creates new form frmBusquedaCliente
     */
    public frmBusquedaCliente(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtBusqueda = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaBusquedaCliente = new javax.swing.JTable();
        buttonAceptar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Busqueda Cliente");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        txtBusqueda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBusquedaKeyReleased(evt);
            }
        });

        tablaBusquedaCliente.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaBusquedaCliente.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaBusquedaClienteMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablaBusquedaCliente);

        buttonAceptar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/accept.png"))); // NOI18N
        buttonAceptar.setEnabled(false);
        buttonAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAceptarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(buttonAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 360, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtBusqueda, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(19, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(txtBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(buttonAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtBusquedaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBusquedaKeyReleased
        String busqueda = txtBusqueda.getText();
        ArrayList<Cliente> aux = new ArrayList<>();
        this.listaClientes = misDatos.generarListaClientes();
        if (busqueda != null) {
            busqueda = busqueda.toLowerCase();
            for (final Cliente i : this.listaClientes) {
                if (Integer.toString(i.getCodigoCliente()).contains(busqueda)
                        || i.getNombre().toLowerCase().contains(busqueda)
                        || i.getApellido().toLowerCase().contains(busqueda)
                        || i.getId().toLowerCase().contains(busqueda)) {
                    aux.add(i);
                }
            }
            
            this.listaClientes = aux;
            
        }
        
        llenarTabla();
                    


    }//GEN-LAST:event_txtBusquedaKeyReleased

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        this.listaClientes = misDatos.generarListaClientes();
        llenarTabla();
    }//GEN-LAST:event_formWindowOpened

    private void tablaBusquedaClienteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaBusquedaClienteMouseClicked
        generarVista(tablaBusquedaCliente.getSelectedRow());
        if (this.clienteSelect != null){
            buttonAceptar.setEnabled(true);
        }else{
            buttonAceptar.setEnabled(false);
        }
    }//GEN-LAST:event_tablaBusquedaClienteMouseClicked

    private void buttonAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAceptarActionPerformed
        generarVista(tablaBusquedaCliente.getSelectedRow());
        txtBusqueda.setText("");
        this.respuesta = this.clienteSelect;
        dispose();
        return;
    }//GEN-LAST:event_buttonAceptarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmBusquedaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmBusquedaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmBusquedaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmBusquedaCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                frmBusquedaCliente dialog = new frmBusquedaCliente(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonAceptar;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tablaBusquedaCliente;
    private javax.swing.JTextField txtBusqueda;
    // End of variables declaration//GEN-END:variables
}
