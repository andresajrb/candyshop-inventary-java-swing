/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package formularios;

import clases.Datos;
import clases.Producto;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author andres
 */
public class frmInventario extends javax.swing.JInternalFrame {

    private Datos misDatos = new Datos();
    private ArrayList<Producto> listaProductos = new ArrayList<>();

    private Producto vistaProducto = new Producto();
    private int cursor = 0;

    // el controlador tendra valor 1 para crear un nuevo producto y 2 para editarlo
    private int controller;

    // auxiliar para el producto que se va a editar
    private int updateCodigo;

    // Tabla de datos
    private DefaultTableModel tablaPrincipal;

    public void setDatos(Datos datos) {
        this.misDatos = datos;
    }

    public void reiniciarLista() {
        this.listaProductos = misDatos.generarListaProductos();
        this.cursor = 0;
        generarVista(this.cursor);
        llenarTabla();
    }

    public int maxCursor() {
        int aux = this.listaProductos.size() - 1;
        return aux;
    }

    public void beginActionButton() {
        // Habilitar Botones
        buttonNuevo.setEnabled(false);
        buttonEditar.setEnabled(false);
        buttonBorrar.setEnabled(false);
        buttonBuscar.setEnabled(false);
        buttonGuardar1.setEnabled(true);
        buttonCancelar.setEnabled(true);
    }

    public void finishActionButton() {
        // Habilitar Botones
        buttonNuevo.setEnabled(true);
        buttonEditar.setEnabled(true);
        buttonBorrar.setEnabled(true);
        buttonBuscar.setEnabled(true);
        buttonGuardar1.setEnabled(false);
        buttonCancelar.setEnabled(false);
    }

    public void enableFields() {
        txtNombre.setEnabled(true);
        txtDescripcion.setEnabled(true);
        txtMarca.setEnabled(true);
        txtCantidad.setEnabled(true);
        txtPrecio.setEnabled(true);
    }

    public void disableFields() {
        txtNombre.setEnabled(false);
        txtDescripcion.setEnabled(false);
        txtMarca.setEnabled(false);
        txtCantidad.setEnabled(false);
        txtPrecio.setEnabled(false);
    }

    public void cleanFields() {
        txtCodigo.setText("");
        txtNombre.setText("");
        txtDescripcion.setText("");
        txtMarca.setText("");
        txtCantidad.setText("0");
        txtPrecio.setText("0");
    }

    private static boolean isNullOrBlank(String s) {
        return (s == null || s.trim().equals(""));
    }

    public String validator() {

        if (isNullOrBlank(txtNombre.getText())) {
            return "Debe ingresar un nombre";

        } else if (isNullOrBlank(txtCantidad.getText())) {
            return "Debe ingresar cantidad";

        } else if (isNullOrBlank(txtPrecio.getText())) {
            return "Debe ingresar u precio";
        } else {
            return null;
        }

    }

    public void generarVista(int auxCursor) {
        if (this.listaProductos.size() > 0) {
            //Si la lista no es vacia muestra el registro de lo contrario lo deja vacio y desabilita 
            //la opcion de editar y borrar
            this.vistaProducto = listaProductos.get(auxCursor);
            txtCodigo.setText(Integer.toString(this.vistaProducto.getCodigoProducto()));
            txtNombre.setText(this.vistaProducto.getNombreProducto());
            txtDescripcion.setText(this.vistaProducto.getDescripcion());
            txtMarca.setText(this.vistaProducto.getMarca());
            txtCantidad.setText(Integer.toString(this.vistaProducto.getCantidad()));
            txtPrecio.setText(Float.toString(this.vistaProducto.getPrecio()));
            buttonBorrar.setEnabled(true);
            buttonEditar.setEnabled(true);
        } else {
            cleanFields();
            disableFields();
            buttonBorrar.setEnabled(false);
            buttonEditar.setEnabled(false);
        }
    }

    public void llenarTabla() {
        String titulos[] = {"Codigo", "Nombre", "Marca", "Cantidad", "Precio"};
        String registro[] = new String[5];
        tablaPrincipal = new DefaultTableModel(null, titulos);

        for (int i = 0; i < this.listaProductos.size(); i++) {
            registro[0] = Integer.toString(this.listaProductos.get(i).getCodigoProducto());
            registro[1] = this.listaProductos.get(i).getNombreProducto();
            registro[2] = this.listaProductos.get(i).getMarca();
            registro[3] = Integer.toString(this.listaProductos.get(i).getCantidad());
            registro[4] = Float.toString(this.listaProductos.get(i).getPrecio());
            tablaPrincipal.addRow(registro);
        }

        tablaProductos.setModel(tablaPrincipal);

    }

    /**
     * Creates new form frmInventario
     */
    public frmInventario() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelCodigo = new javax.swing.JLabel();
        labelNombre = new javax.swing.JLabel();
        labelDescripcion = new javax.swing.JLabel();
        labelMarca = new javax.swing.JLabel();
        labelCantidad = new javax.swing.JLabel();
        labelPrecio = new javax.swing.JLabel();
        txtCodigo = new javax.swing.JTextField();
        txtNombre = new javax.swing.JTextField();
        txtMarca = new javax.swing.JTextField();
        txtCantidad = new javax.swing.JTextField();
        txtPrecio = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDescripcion = new javax.swing.JTextArea();
        buttonBorrar = new javax.swing.JButton();
        buttonNuevo = new javax.swing.JButton();
        buttonBuscar = new javax.swing.JButton();
        buttonGuardar1 = new javax.swing.JButton();
        buttonCancelar = new javax.swing.JButton();
        buttonEditar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaProductos = new javax.swing.JTable();

        setClosable(true);
        setTitle("Inventario");
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
        });

        labelCodigo.setText("Codigo");

        labelNombre.setText("Nombre");

        labelDescripcion.setText("Descripcion");

        labelMarca.setText("Marca");

        labelCantidad.setText("Cantidad");

        labelPrecio.setText("Precio");

        txtCodigo.setEnabled(false);

        txtNombre.setEnabled(false);

        txtMarca.setEnabled(false);

        txtCantidad.setEnabled(false);

        txtPrecio.setEnabled(false);

        jScrollPane1.setEnabled(false);

        txtDescripcion.setColumns(20);
        txtDescripcion.setRows(5);
        txtDescripcion.setEnabled(false);
        jScrollPane1.setViewportView(txtDescripcion);

        buttonBorrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/delete.png"))); // NOI18N
        buttonBorrar.setToolTipText("Borrar registro");
        buttonBorrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonBorrarActionPerformed(evt);
            }
        });

        buttonNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/add.png"))); // NOI18N
        buttonNuevo.setToolTipText("Crear un nuevo registro");
        buttonNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNuevoActionPerformed(evt);
            }
        });

        buttonBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
        buttonBuscar.setToolTipText("Buscar registro");
        buttonBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonBuscarActionPerformed(evt);
            }
        });

        buttonGuardar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/save.png"))); // NOI18N
        buttonGuardar1.setToolTipText("Guardar cambios");
        buttonGuardar1.setEnabled(false);
        buttonGuardar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonGuardar1ActionPerformed(evt);
            }
        });

        buttonCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/cancel2.png"))); // NOI18N
        buttonCancelar.setToolTipText("Cancelar operacion");
        buttonCancelar.setEnabled(false);
        buttonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCancelarActionPerformed(evt);
            }
        });

        buttonEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/edit.png"))); // NOI18N
        buttonEditar.setToolTipText("Editar registro");
        buttonEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonEditarActionPerformed(evt);
            }
        });

        tablaProductos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaProductos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaProductosMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tablaProductos);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 31, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(buttonNuevo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonEditar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonGuardar1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonCancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonBorrar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonBuscar)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(labelCantidad)
                            .addComponent(labelCodigo)
                            .addComponent(labelNombre)
                            .addComponent(labelMarca))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtCantidad, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
                                    .addComponent(txtCodigo, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(labelDescripcion))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txtNombre, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                                    .addComponent(txtMarca))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(labelPrecio)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(15, 15, 15))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(labelCodigo)
                            .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(labelDescripcion))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(labelNombre)
                            .addComponent(txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelMarca)
                    .addComponent(txtMarca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPrecio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelPrecio))
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelCantidad)
                    .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonNuevo)
                    .addComponent(buttonEditar)
                    .addComponent(buttonBorrar)
                    .addComponent(buttonGuardar1)
                    .addComponent(buttonCancelar)
                    .addComponent(buttonBuscar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buttonBorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonBorrarActionPerformed

        int confirmacion = JOptionPane.showConfirmDialog(rootPane,
                "¿Esta seguro que desea eliminar el prodcto "
                + vistaProducto.getNombreProducto() + " ?");

        boolean sqlError;

        if (confirmacion == 0) {
            sqlError = misDatos.deleteProduct(vistaProducto.getCodigoProducto());
            if (sqlError) {
                JOptionPane.showMessageDialog(rootPane, "El producto "
                        + vistaProducto.getNombreProducto()
                        + " fue eliminado exitosamente");
                reiniciarLista();
            } else {
                JOptionPane.showMessageDialog(rootPane, "Hubo un error al eliminar"
                        + " el producto, por favor notifique al analista de IT");
            }
        }
    }//GEN-LAST:event_buttonBorrarActionPerformed

    private void buttonNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNuevoActionPerformed
        beginActionButton();
        enableFields();
        cleanFields();
        this.controller = 1;
    }//GEN-LAST:event_buttonNuevoActionPerformed

    private void buttonBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonBuscarActionPerformed
        String busqueda = JOptionPane.showInputDialog(rootPane, "Ingrese palabra a buscar", "Busqueda", -1);
        reiniciarLista();
        ArrayList<Producto> aux = new ArrayList<>();
        if (busqueda != null) {
            busqueda = busqueda.toLowerCase();
            for (final Producto i : this.listaProductos) {
                if (Integer.toString(i.getCodigoProducto()).contains(busqueda)
                        || i.getNombreProducto().toLowerCase().contains(busqueda)) {
                    aux.add(i);
                }
            }

            if (aux.isEmpty()) {
                JOptionPane.showMessageDialog(rootPane, "No se encontraron resultados");
            } else {
                this.listaProductos = aux;
                generarVista(0);
                llenarTabla();
            }
        }
    }//GEN-LAST:event_buttonBuscarActionPerformed

    private void buttonGuardar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonGuardar1ActionPerformed

        boolean sqlError;
        String error;

        error = validator();

        if (error == null) {

            if (controller == 1) {
                Producto newProduct
                        = new Producto(0, txtNombre.getText(), txtDescripcion.getText(),
                                txtMarca.getText(), Integer.parseInt(txtCantidad.getText()),
                                Float.parseFloat(txtPrecio.getText()));

                sqlError = misDatos.createNewProduct(newProduct);

                if (sqlError) {
                    JOptionPane.showMessageDialog(rootPane, "El Producto "
                            + txtNombre.getText()
                            + " fue agregado exitosamente");

                    finishActionButton();
                    disableFields();
                    reiniciarLista();

                } else {
                    JOptionPane.showMessageDialog(rootPane, "Hubo un error al ingresar"
                            + " el producto, por favor notifique al analista de IT");
                }

            } else {
                Producto updateProduct
                        = new Producto(0, txtNombre.getText(), txtDescripcion.getText(),
                                txtMarca.getText(), Integer.parseInt(txtCantidad.getText()),
                                Float.parseFloat(txtPrecio.getText()));

                sqlError = misDatos.updateProduct(updateProduct, this.updateCodigo);

                if (sqlError) {
                    JOptionPane.showMessageDialog(rootPane, "El Producto "
                            + txtNombre.getText() + " fue actualizado exitosamente");

                    finishActionButton();
                    disableFields();
                    reiniciarLista();

                } else {
                    JOptionPane.showMessageDialog(rootPane, "Hubo un error al actualizar"
                            + " el producto, por favor notifique al analista de IT");
                }
            }

        } else {
            JOptionPane.showMessageDialog(rootPane, error, "Error al ingresar", 0);
        }
    }//GEN-LAST:event_buttonGuardar1ActionPerformed

    private void buttonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCancelarActionPerformed
        finishActionButton();
        disableFields();
        reiniciarLista();
    }//GEN-LAST:event_buttonCancelarActionPerformed

    private void buttonEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonEditarActionPerformed
        beginActionButton();
        enableFields();
        this.controller = 2;
        updateCodigo = Integer.parseInt(txtCodigo.getText());
    }//GEN-LAST:event_buttonEditarActionPerformed

    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened
        reiniciarLista();
    }//GEN-LAST:event_formInternalFrameOpened

    private void tablaProductosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaProductosMouseClicked
        this.cursor = tablaProductos.getSelectedRow();
        generarVista(this.cursor);
    }//GEN-LAST:event_tablaProductosMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonBorrar;
    private javax.swing.JButton buttonBuscar;
    private javax.swing.JButton buttonCancelar;
    private javax.swing.JButton buttonEditar;
    private javax.swing.JButton buttonGuardar1;
    private javax.swing.JButton buttonNuevo;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel labelCantidad;
    private javax.swing.JLabel labelCodigo;
    private javax.swing.JLabel labelDescripcion;
    private javax.swing.JLabel labelMarca;
    private javax.swing.JLabel labelNombre;
    private javax.swing.JLabel labelPrecio;
    private javax.swing.JTable tablaProductos;
    private javax.swing.JTextField txtCantidad;
    private javax.swing.JTextField txtCodigo;
    private javax.swing.JTextArea txtDescripcion;
    private javax.swing.JTextField txtMarca;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtPrecio;
    // End of variables declaration//GEN-END:variables
}
