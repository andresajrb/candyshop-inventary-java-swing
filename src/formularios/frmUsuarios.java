package formularios;

import clases.Datos;
import clases.Usuario;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

//Formulario para el mantenimiento de la tabla USUARIOS de la Base de Datos
public class frmUsuarios extends javax.swing.JInternalFrame {

    //Variable datos para la comunicacion con la Base de Datos
    private Datos misDatos = new Datos();
    //Variable que se utiliza para mostrar el usuario para el CRUD
    private Usuario vistaUsuario = new Usuario();
    //Variable con la que se ubica el usuario en la lista de usuarios de la clase Datos
    private int cursor = 0;

    // el controlador tendra valor 1 para crear un nuevo usuario y 2 para editarlo
    private int controller;

    // auxiliar para el usuario que se va a editar
    private String updateId;

    // Tabla de datos
    private DefaultTableModel tablaPrincipal;

    public void setDatos(Datos datos) {
        this.misDatos = datos;
        generarVista(this.cursor);
    }

    //Vuelve a generar la lista de Usuarios de la clase Datos
    public void reiniciarLista() {
        misDatos.generarListaUsuarios();
        this.cursor = 0;
        generarVista(this.cursor);
        llenarTabla();
    }

    //Selecciona un valor de la lista de Usuarios de la clase Datos y la muestra en pantalla
    public void generarVista(int auxCursor) {
        //Si la lista no es vacia muestra el registro de lo contrario lo deja vacio y desabilita 
        //la opcion de editar y borrar
        if (misDatos.getListaUsuarios().size() > 0) {
            this.vistaUsuario = misDatos.getListaUsuarios().get(auxCursor);
            txtIdUsuario.setText(vistaUsuario.getIdUsuario());
            txtNombres.setText(vistaUsuario.getNombre());
            txtApellidos.setText(vistaUsuario.getApellido());
            txtClave.setText(vistaUsuario.getClave());
            txtConfirmacion.setText(vistaUsuario.getClave());
            comboPerfil.setSelectedIndex(vistaUsuario.getPerfil());
            buttonBorrar.setEnabled(true);
            buttonEditar.setEnabled(true);
        } else {
            // Limpiar campos
            txtIdUsuario.setText("");
            txtNombres.setText("");
            txtApellidos.setText("");
            txtClave.setText("");
            txtConfirmacion.setText("");
            comboPerfil.setSelectedIndex(0);
            buttonBorrar.setEnabled(false);
            buttonEditar.setEnabled(false);
        }

    }

    //Este metodo restorna la ultima posicion de la lista de usuarios
    public int maxCursor() {
        int aux = this.misDatos.getListaUsuarios().size() - 1;
        return aux;
    }

    //Herramienta para ubicar valores NULL o vacios
    private static boolean isNullOrBlank(String s) {
        return (s == null || s.trim().equals(""));
    }

    //Validaciones Directas
    public int validator() {

        String auxPassword = new String(txtClave.getPassword());
        String auxConfirmacion = new String(txtConfirmacion.getPassword());

        if (isNullOrBlank(txtIdUsuario.getText())) {
            return 2;
        }
        if (!auxPassword.equals(auxConfirmacion)) {
            return 3;
        }
        if (comboPerfil.getSelectedIndex() == 0) {
            return 4;
        }
        return 0;
    }

    //Metodo que llena la tabla de datos en la pantalla
    public void llenarTabla() {
        String titulos[] = {"ID Usuario", "Nombres", "Apellidos", "Perfil"};
        String registro[] = new String[4];
        tablaPrincipal = new DefaultTableModel(null, titulos);

        for (int i = 0; i < misDatos.getListaUsuarios().size(); i++) {
            registro[0] = misDatos.getListaUsuarios().get(i).getIdUsuario();
            registro[1] = misDatos.getListaUsuarios().get(i).getNombre();
            registro[2] = misDatos.getListaUsuarios().get(i).getApellido();
            registro[3] = obtenerPerfil(misDatos.getListaUsuarios().get(i).getPerfil());
            tablaPrincipal.addRow(registro);
        }

        tablaUsuarios.setModel(tablaPrincipal);

    }

    //Devuelve Administrador cuando el valor perfil es igual a 1 de lo contrario retorna Empleado
    public String obtenerPerfil(int perfil) {
        if (perfil == 1) {
            return "Administrador";
        } else {
            return "Empleado";
        }
    }

    //Devuelve los botones a su estado original
    public void restaurarBotones() {
        // Habilitar Botones
        buttonPrimero.setEnabled(true);
        buttonAnterior.setEnabled(true);
        buttonSiguiente.setEnabled(true);
        buttonUltimo.setEnabled(true);
        buttonNuevo.setEnabled(true);
        buttonEditar.setEnabled(true);
        buttonBorrar.setEnabled(true);
        buttonBuscar.setEnabled(true);
        buttonGuardar1.setEnabled(false);
        buttonCancelar.setEnabled(false);
        // Deshabilitar campos de texto
        txtIdUsuario.setEnabled(false);
        txtNombres.setEnabled(false);
        txtApellidos.setEnabled(false);
        txtClave.setEnabled(false);
        txtConfirmacion.setEnabled(false);
        comboPerfil.setEnabled(false);
    }

    //Getters y Setters
    public int getController() {
        return controller;
    }

    public void setController(int controller) {
        this.controller = controller;
    }

    public String getUpdateId() {
        return updateId;
    }

    public void setUpdateId(String updateId) {
        this.updateId = updateId;
    }

    /**
     * Creates new form NewJInternalFrame
     */
    public frmUsuarios() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelUsuario = new javax.swing.JLabel();
        txtIdUsuario = new javax.swing.JTextField();
        labelUsuario1 = new javax.swing.JLabel();
        txtNombres = new javax.swing.JTextField();
        labelUsuario2 = new javax.swing.JLabel();
        txtApellidos = new javax.swing.JTextField();
        labelUsuario3 = new javax.swing.JLabel();
        txtClave = new javax.swing.JPasswordField();
        labelUsuario4 = new javax.swing.JLabel();
        txtConfirmacion = new javax.swing.JPasswordField();
        labelUsuario5 = new javax.swing.JLabel();
        comboPerfil = new javax.swing.JComboBox<>();
        buttonPrimero = new javax.swing.JButton();
        buttonSiguiente = new javax.swing.JButton();
        buttonAnterior = new javax.swing.JButton();
        buttonUltimo = new javax.swing.JButton();
        buttonCancelar = new javax.swing.JButton();
        buttonEditar = new javax.swing.JButton();
        buttonBorrar = new javax.swing.JButton();
        buttonNuevo = new javax.swing.JButton();
        buttonBuscar = new javax.swing.JButton();
        buttonGuardar1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaUsuarios = new javax.swing.JTable();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Usuarios");
        setToolTipText("");
        setEnabled(false);
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
        });

        labelUsuario.setText("ID Usuario:");

        txtIdUsuario.setEnabled(false);

        labelUsuario1.setText("Nombres:");

        txtNombres.setEnabled(false);

        labelUsuario2.setText("Apellidos:");

        txtApellidos.setEnabled(false);
        txtApellidos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtApellidosActionPerformed(evt);
            }
        });

        labelUsuario3.setText("Clave:");

        txtClave.setEnabled(false);

        labelUsuario4.setText("Confirmacion:");

        txtConfirmacion.setEnabled(false);

        labelUsuario5.setText("Perfil:");

        comboPerfil.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione un perfil", "Administrador", "Empleado" }));
        comboPerfil.setEnabled(false);

        buttonPrimero.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/if_BT_double_arrow_left_905528.png"))); // NOI18N
        buttonPrimero.setToolTipText("Va al primer registro");
        buttonPrimero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonPrimeroActionPerformed(evt);
            }
        });

        buttonSiguiente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/if_BT_arrow_right_905535.png"))); // NOI18N
        buttonSiguiente.setToolTipText("Va al siguiente registro");
        buttonSiguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSiguienteActionPerformed(evt);
            }
        });

        buttonAnterior.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/if_BT_arrow_left_905524.png"))); // NOI18N
        buttonAnterior.setToolTipText("Va al registro anterior");
        buttonAnterior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAnteriorActionPerformed(evt);
            }
        });

        buttonUltimo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/if_BT_double_arrow_right_905527.png"))); // NOI18N
        buttonUltimo.setToolTipText("Va al ultimo registro");
        buttonUltimo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonUltimoActionPerformed(evt);
            }
        });

        buttonCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/cancel2.png"))); // NOI18N
        buttonCancelar.setToolTipText("Cancelar operacion");
        buttonCancelar.setEnabled(false);
        buttonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCancelarActionPerformed(evt);
            }
        });

        buttonEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/edit.png"))); // NOI18N
        buttonEditar.setToolTipText("Editar registro");
        buttonEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonEditarActionPerformed(evt);
            }
        });

        buttonBorrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/delete.png"))); // NOI18N
        buttonBorrar.setToolTipText("Borrar registro");
        buttonBorrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonBorrarActionPerformed(evt);
            }
        });

        buttonNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/add.png"))); // NOI18N
        buttonNuevo.setToolTipText("Crear un nuevo registro");
        buttonNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNuevoActionPerformed(evt);
            }
        });

        buttonBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
        buttonBuscar.setToolTipText("Buscar registro");
        buttonBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonBuscarActionPerformed(evt);
            }
        });

        buttonGuardar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/save.png"))); // NOI18N
        buttonGuardar1.setToolTipText("Guardar cambios");
        buttonGuardar1.setEnabled(false);
        buttonGuardar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonGuardar1ActionPerformed(evt);
            }
        });

        tablaUsuarios.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaUsuarios.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        tablaUsuarios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaUsuariosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablaUsuarios);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(labelUsuario1, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(labelUsuario2, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(labelUsuario3, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(labelUsuario, javax.swing.GroupLayout.Alignment.TRAILING))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(txtIdUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(labelUsuario5)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(comboPerfil, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(txtClave, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(68, 68, 68)
                                            .addComponent(labelUsuario4)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(txtConfirmacion, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(txtApellidos, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 350, Short.MAX_VALUE)
                                            .addComponent(txtNombres, javax.swing.GroupLayout.Alignment.LEADING)))))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(buttonPrimero)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(buttonAnterior)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(buttonSiguiente)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(buttonUltimo))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(buttonNuevo)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(buttonEditar)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(buttonGuardar1)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(buttonCancelar)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(buttonBorrar)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(buttonBuscar))))))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 431, Short.MAX_VALUE)
                        .addGap(11, 11, 11)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(labelUsuario)
                        .addComponent(txtIdUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(labelUsuario5))
                    .addComponent(comboPerfil, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelUsuario1)
                    .addComponent(txtNombres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelUsuario2)
                    .addComponent(txtApellidos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelUsuario3)
                    .addComponent(txtClave, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelUsuario4)
                    .addComponent(txtConfirmacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonPrimero)
                    .addComponent(buttonAnterior)
                    .addComponent(buttonSiguiente)
                    .addComponent(buttonUltimo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonNuevo)
                    .addComponent(buttonEditar)
                    .addComponent(buttonBorrar)
                    .addComponent(buttonGuardar1)
                    .addComponent(buttonCancelar)
                    .addComponent(buttonBuscar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtApellidosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtApellidosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtApellidosActionPerformed

    private void buttonPrimeroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonPrimeroActionPerformed
        generarVista(0);
    }//GEN-LAST:event_buttonPrimeroActionPerformed

    private void buttonNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNuevoActionPerformed
        // Habilitar Botones
        buttonPrimero.setEnabled(false);
        buttonAnterior.setEnabled(false);
        buttonSiguiente.setEnabled(false);
        buttonUltimo.setEnabled(false);
        buttonNuevo.setEnabled(false);
        buttonEditar.setEnabled(false);
        buttonBorrar.setEnabled(false);
        buttonBuscar.setEnabled(false);
        buttonGuardar1.setEnabled(true);
        buttonCancelar.setEnabled(true);
        // Habilitar campos de texto
        txtIdUsuario.setEnabled(true);
        txtNombres.setEnabled(true);
        txtApellidos.setEnabled(true);
        txtClave.setEnabled(true);
        txtConfirmacion.setEnabled(true);
        comboPerfil.setEnabled(true);
        // Limpiar campos
        txtIdUsuario.setText("");
        txtNombres.setText("");
        txtApellidos.setText("");
        txtClave.setText("");
        txtConfirmacion.setText("");
        comboPerfil.setSelectedIndex(0);
        // Se coloca el cursos en el ID
        txtIdUsuario.requestFocusInWindow();
        // Se establece el controlador en 1
        setController(1);


    }//GEN-LAST:event_buttonNuevoActionPerformed

    private void buttonGuardar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonGuardar1ActionPerformed
        //variable para guardar errores de las validaciones
        int error = 0;
        //Validacion utilizando la Base de Datos
        if (getController() == 1) {
            error = misDatos.validacionIdUsuario(txtIdUsuario.getText());
            if (error == 1) {
                JOptionPane.showMessageDialog(rootPane, "El ID: "
                        + txtIdUsuario.getText() + " ya existe, por favor ingrese otro");
                txtIdUsuario.requestFocusInWindow();
                return;
            }
        }
        //Uso de el metodo para validar los valores
        error = validator();

        if (error == 2) {
            JOptionPane.showMessageDialog(rootPane, "El campo ID Usuario no puede ser vacio");
            txtIdUsuario.requestFocusInWindow();
            return;
        }
        if (error == 3) {
            JOptionPane.showMessageDialog(rootPane, "La confirmacion no es igual a la clave");
            txtConfirmacion.requestFocusInWindow();
            return;
        }
        if (error == 4) {
            JOptionPane.showMessageDialog(rootPane, "Por favor seleccione un perfil");
            return;
        }

        //Convierte el dato en el campo Password a String
        String auxPassword = new String(txtClave.getPassword());
        //Utiliza un auxiliar para crear o editar un registro en la tabla USUARIOS
        Usuario auxUser = new Usuario(txtIdUsuario.getText(), txtNombres.getText(),
                txtApellidos.getText(), auxPassword, comboPerfil.getSelectedIndex());

        //Variable que guarda los errores de la comunicacion con la Base de Datos
        boolean sqlError;
        //Si Controller es igual a 1 se crea un nuevo registro en la tabla USUARIOS
        if (getController() == 1) {

            sqlError = misDatos.createNewUser(auxUser);

            if (sqlError) {

                JOptionPane.showMessageDialog(rootPane, "El usuario " + txtIdUsuario.getText() + " fue agregado exitosamente");

                restaurarBotones();
                // Genero la lista de nuevo con el cursor en el primero
                reiniciarLista();
            } else {
                JOptionPane.showMessageDialog(rootPane, "Hubo un error al ingresar"
                        + " el usuario, por favor notifique al analista de IT");
            }
        } else {
            sqlError = misDatos.updateUser(auxUser, getUpdateId());

            if (sqlError) {
                JOptionPane.showMessageDialog(rootPane, "El usuario "
                        + getUpdateId() + " fue actualizado exitosamente");

                restaurarBotones();
                // Genero la lista de nuevo con el cursor en el primero
                reiniciarLista();
            } else {
                JOptionPane.showMessageDialog(rootPane, "Hubo un error al actualizar"
                        + " el usuario, por favor notifique al analista de IT");
            }
        }
    }//GEN-LAST:event_buttonGuardar1ActionPerformed

    private void buttonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCancelarActionPerformed
        restaurarBotones();
        // Genero la lista de nuevo con el cursor en el primero
        reiniciarLista();
    }//GEN-LAST:event_buttonCancelarActionPerformed

    private void buttonEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonEditarActionPerformed
        // Habilitar Botones
        buttonPrimero.setEnabled(false);
        buttonAnterior.setEnabled(false);
        buttonSiguiente.setEnabled(false);
        buttonUltimo.setEnabled(false);
        buttonNuevo.setEnabled(false);
        buttonEditar.setEnabled(false);
        buttonBorrar.setEnabled(false);
        buttonBuscar.setEnabled(false);
        buttonGuardar1.setEnabled(true);
        buttonCancelar.setEnabled(true);
        // Habilitar campos de texto
        txtNombres.setEnabled(true);
        txtApellidos.setEnabled(true);
        txtClave.setEnabled(true);
        txtConfirmacion.setEnabled(true);
        comboPerfil.setEnabled(true);
        // Se coloca el cursos en el ID
        txtNombres.requestFocusInWindow();
        // Se establece el controlador en 2
        setController(2);
        setUpdateId(vistaUsuario.getIdUsuario());
    }//GEN-LAST:event_buttonEditarActionPerformed

    private void buttonAnteriorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAnteriorActionPerformed
        //Ajusta el valor del cursor para mostrar el dato anterior si este no es el primero
        if (this.cursor != 0) {
            this.cursor = this.cursor - 1;
        }
        //Se muestra el usuario en pantalla
        generarVista(this.cursor);
    }//GEN-LAST:event_buttonAnteriorActionPerformed

    private void buttonUltimoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonUltimoActionPerformed
        //Muestra el ultimo registro en la lista de usuarios en pantalla
        generarVista(maxCursor());
    }//GEN-LAST:event_buttonUltimoActionPerformed

    private void buttonSiguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSiguienteActionPerformed
        //Ajusta el valor del cursor para mostrar el dato siguiente si este no es el ultimo
        if (this.cursor != maxCursor()) {
            this.cursor = this.cursor + 1;
        }

        //Muestra el usuario en pantalla
        generarVista(this.cursor);
    }//GEN-LAST:event_buttonSiguienteActionPerformed

    private void buttonBorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonBorrarActionPerformed
        //Confirma que se desea eliminar el registro de la tabla USUARIOS
        int confirmacion = JOptionPane.showConfirmDialog(rootPane,
                "¿Esta seguro que desea eliminar el usuario "
                + vistaUsuario.getIdUsuario() + " ?");

        //Guarda el error proveniente de la comunicacion con la Base de Datos
        boolean sqlError;

        //Si confirmacion es igual a 0, se eliminar el registro de la Base de Datos
        if (confirmacion == 0) {
            sqlError = misDatos.deleteUser(vistaUsuario.getIdUsuario());
            if (sqlError) {
                JOptionPane.showMessageDialog(rootPane, "El usuario "
                        + vistaUsuario.getIdUsuario() + " fue eliminado exitosamente");
                reiniciarLista();
            } else {
                JOptionPane.showMessageDialog(rootPane, "Hubo un error al eliminar"
                        + " el usuario, por favor notifique al analista de IT");
            }
        }
    }//GEN-LAST:event_buttonBorrarActionPerformed

    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened
        //Al entrar en la pantalla se cargan los datos en la tabla
        llenarTabla();
    }//GEN-LAST:event_formInternalFrameOpened

    private void tablaUsuariosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaUsuariosMouseClicked
        //Si se selecciona un valor de la tabla este es mostrado en pantalla
        restaurarBotones();
        generarVista(tablaUsuarios.getSelectedRow());
    }//GEN-LAST:event_tablaUsuariosMouseClicked

    private void buttonBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonBuscarActionPerformed
        //Se ingresa la palabra clave en la busqueda
        String busqueda = JOptionPane.showInputDialog(rootPane, "Ingrese palabra a buscar", "Busqueda", -1);
        //Vuelve a generar toda la lista de usuarios en la clase Datos
        reiniciarLista();
        //Esta variable se utilizara para encontrar los registros que contengan la palabra clave
        ArrayList<Usuario> aux = new ArrayList<>();
        if (busqueda != null) {
            busqueda = busqueda.toLowerCase();
            for (final Usuario i : misDatos.getListaUsuarios()) {
                if (i.getIdUsuario().contains(busqueda)
                        || i.getNombre().toLowerCase().contains(busqueda)
                        || i.getApellido().toLowerCase().contains(busqueda)) {
                    aux.add(i);
                }
            }

            //Si ningun dato coincide con la busqueda devuelve un mensaje de error y reinicia la tabla
            if (aux.isEmpty()) {
                JOptionPane.showMessageDialog(rootPane, "No se encontraron resultados");
            } else {
                misDatos.setListaUsuarios(aux);
                generarVista(0);
                llenarTabla();
            }
        }
    }//GEN-LAST:event_buttonBuscarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonAnterior;
    private javax.swing.JButton buttonBorrar;
    private javax.swing.JButton buttonBuscar;
    private javax.swing.JButton buttonCancelar;
    private javax.swing.JButton buttonEditar;
    private javax.swing.JButton buttonGuardar1;
    private javax.swing.JButton buttonNuevo;
    private javax.swing.JButton buttonPrimero;
    private javax.swing.JButton buttonSiguiente;
    private javax.swing.JButton buttonUltimo;
    private javax.swing.JComboBox<String> comboPerfil;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelUsuario;
    private javax.swing.JLabel labelUsuario1;
    private javax.swing.JLabel labelUsuario2;
    private javax.swing.JLabel labelUsuario3;
    private javax.swing.JLabel labelUsuario4;
    private javax.swing.JLabel labelUsuario5;
    private javax.swing.JTable tablaUsuarios;
    private javax.swing.JTextField txtApellidos;
    private javax.swing.JPasswordField txtClave;
    private javax.swing.JPasswordField txtConfirmacion;
    private javax.swing.JTextField txtIdUsuario;
    private javax.swing.JTextField txtNombres;
    // End of variables declaration//GEN-END:variables
}
