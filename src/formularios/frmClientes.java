package formularios;

import clases.Cliente;
import clases.Datos;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

//Ventana para el mantenimiento de la tabla CLIENTES
public class frmClientes extends javax.swing.JInternalFrame {

    //Variable para la comunicacion con la Base de Datos
    private Datos misDatos = new Datos();
    //Variable para guardar la lista de los registros de la tabla CLIENTES de la BD
    private ArrayList<Cliente> listaClientes = new ArrayList<>();
    //Variable para mostrar en pantalla el registro de cliente para realizar los CRUD
    private Cliente vistaCliente = new Cliente();
    //Variable para establecer la posicion en la lista de clientes
    private int cursor = 0;

    // el controlador tendra valor 1 para crear un nuevo usuario y 2 para editarlo
    private int controller;

    // auxiliar para el usuario que se va a editar
    private int updateCodigo;

    // Tabla de datos
    private DefaultTableModel tablaPrincipal;

    public void setDatos(Datos datos) {
        this.misDatos = datos;
    }

    //Transforma lo establecido en el ComboBox a un valor entero
    public int getComboFromID(String ID) {
        String subString = ID.substring(0, 1);

        if (subString.equalsIgnoreCase("V")) {
            return 1;
        } else if (subString.equalsIgnoreCase("E")) {
            return 2;
        } else {
            return 3;
        }

    }

    //Transforma en numero entero a su equivalente String para mostrarlo en el ComboBox
    public String getIDFromCombo() {

        switch (comboID.getSelectedIndex()) {
            case 1:
                return "V" + txtID.getText();
            case 2:
                return "E" + txtID.getText();
            case 3:
                return "J" + txtID.getText();
            default:
                return "";
        }
    }

    //Reinicia los valores de la lista de clientes y los obtiene de la tabla CLIENTES
    public void reiniciarLista() {
        this.listaClientes = misDatos.generarListaClientes();
        this.cursor = 0;
        generarVista(this.cursor);
        llenarTabla();
    }

    //Obtiene el indice maximo de la lista de clientes
    public int maxCursor() {
        int aux = this.listaClientes.size() - 1;
        return aux;
    }

    //Maneja la habilitacion de los botones
    public void beginActionButton() {
        // Habilitar Botones
        buttonNuevo.setEnabled(false);
        buttonEditar.setEnabled(false);
        buttonBorrar.setEnabled(false);
        buttonBuscar.setEnabled(false);
        buttonGuardar1.setEnabled(true);
        buttonCancelar.setEnabled(true);
    }

    //Maneja la habilitacion de los botones
    public void finishActionButton() {
        // Habilitar Botones
        buttonNuevo.setEnabled(true);
        buttonEditar.setEnabled(true);
        buttonBorrar.setEnabled(true);
        buttonBuscar.setEnabled(true);
        buttonGuardar1.setEnabled(false);
        buttonCancelar.setEnabled(false);
    }

    //Maneja la habilitacion de los campos
    public void enableFields() {
        txtNombres.setEnabled(true);
        txtApellidos.setEnabled(true);
        txtTelefono.setEnabled(true);
        txtDireccion.setEnabled(true);
        comboID.setEnabled(true);
        txtID.setEnabled(true);
    }

    //Maneja la habilitacion de los campos
    public void disableFields() {
        txtNombres.setEnabled(false);
        txtApellidos.setEnabled(false);
        txtTelefono.setEnabled(false);
        txtDireccion.setEnabled(false);
        comboID.setEnabled(false);
        txtID.setEnabled(false);
    }

    //Limpia los campos de texto en la pantalla
    public void cleanFields() {
        txtCodigo.setText("");
        txtNombres.setText("");
        txtApellidos.setText("");
        txtTelefono.setText("");
        txtDireccion.setText("");
        comboID.setSelectedIndex(0);
        txtID.setText("");
    }

    //Herramienta para encontrar valores null o vacios
    private static boolean isNullOrBlank(String s) {
        return (s == null || s.trim().equals(""));
    }

    //Validaciones directas
    public String validator() {
        //campo nombre vacio
        if (isNullOrBlank(txtNombres.getText())) {
            return "Debe ingresar un nombre";
            //campo apellido vacio
        } else if (isNullOrBlank(txtApellidos.getText())) {
            return "Debe ingresar un apellido";
            //campo ID vacio
        } else if (isNullOrBlank(txtID.getText())) {
            return "Debe ingresar un ID";
            //no selecciono el tipo de ID
        } else if (comboID.getSelectedIndex() == 0) {
            return "Debe seleccionar un tipo de ID";
        } else {
            return null;
        }

    }

    //Muestra en la pantalla el registro con el indice de la lista ingresado como parametro
    public void generarVista(int auxCursor) {
        //Si la lista no es vacia muestra el registro de lo contrario lo deja vacio y desabilita 
        //la opcion de editar y borrar
        if (this.listaClientes.size() > 0) {
            this.vistaCliente = listaClientes.get(auxCursor);
            txtCodigo.setText(Integer.toString(this.vistaCliente.getCodigoCliente()));
            txtNombres.setText(this.vistaCliente.getNombre());
            txtApellidos.setText(this.vistaCliente.getApellido());
            txtTelefono.setText(this.vistaCliente.getTelefono());
            txtDireccion.setText(this.vistaCliente.getDireccion());
            comboID.setSelectedIndex(getComboFromID(this.vistaCliente.getId()));
            txtID.setText(this.vistaCliente.getId().substring(1));
            buttonBorrar.setEnabled(true);
            buttonEditar.setEnabled(true);
        } else {
            cleanFields();
            disableFields();
            buttonBorrar.setEnabled(false);
            buttonEditar.setEnabled(false);
        }
    }

    //Llena la tabla de datos con la lista de clientes obtenida de la base de datos
    public void llenarTabla() {
        String titulos[] = {"Codigo", "Nombres", "Apellidos", "Telefono", "ID"};
        String registro[] = new String[5];
        tablaPrincipal = new DefaultTableModel(null, titulos);

        for (int i = 0; i < this.listaClientes.size(); i++) {
            registro[0] = Integer.toString(this.listaClientes.get(i).getCodigoCliente());
            registro[1] = this.listaClientes.get(i).getNombre();
            registro[2] = this.listaClientes.get(i).getApellido();
            registro[3] = this.listaClientes.get(i).getTelefono();
            registro[4] = this.listaClientes.get(i).getId();
            tablaPrincipal.addRow(registro);
        }

        tablaClientes.setModel(tablaPrincipal);

    }

    public frmClientes() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelCodigo = new javax.swing.JLabel();
        labelNombre = new javax.swing.JLabel();
        labelTelefono = new javax.swing.JLabel();
        labelApellido = new javax.swing.JLabel();
        labelDireccion = new javax.swing.JLabel();
        labelID = new javax.swing.JLabel();
        txtCodigo = new javax.swing.JTextField();
        txtNombres = new javax.swing.JTextField();
        txtApellidos = new javax.swing.JTextField();
        txtTelefono = new javax.swing.JTextField();
        txtID = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDireccion = new javax.swing.JTextArea();
        buttonBuscar = new javax.swing.JButton();
        buttonGuardar1 = new javax.swing.JButton();
        buttonCancelar = new javax.swing.JButton();
        buttonEditar = new javax.swing.JButton();
        buttonBorrar = new javax.swing.JButton();
        buttonNuevo = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaClientes = new javax.swing.JTable();
        comboID = new javax.swing.JComboBox<>();

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Clientes");
        setToolTipText("");
        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
        });

        labelCodigo.setText("Codigo");

        labelNombre.setText("Nombres");

        labelTelefono.setText("Telefono");

        labelApellido.setText("Apellidos");

        labelDireccion.setText("Direccion");

        labelID.setText("ID");

        txtCodigo.setEnabled(false);

        txtNombres.setEnabled(false);

        txtApellidos.setEnabled(false);

        txtTelefono.setEnabled(false);

        txtID.setEnabled(false);

        jScrollPane1.setEnabled(false);

        txtDireccion.setColumns(20);
        txtDireccion.setRows(5);
        txtDireccion.setEnabled(false);
        jScrollPane1.setViewportView(txtDireccion);

        buttonBuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/search.png"))); // NOI18N
        buttonBuscar.setToolTipText("Buscar registro");
        buttonBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonBuscarActionPerformed(evt);
            }
        });

        buttonGuardar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/save.png"))); // NOI18N
        buttonGuardar1.setToolTipText("Guardar cambios");
        buttonGuardar1.setEnabled(false);
        buttonGuardar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonGuardar1ActionPerformed(evt);
            }
        });

        buttonCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/cancel2.png"))); // NOI18N
        buttonCancelar.setToolTipText("Cancelar operacion");
        buttonCancelar.setEnabled(false);
        buttonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonCancelarActionPerformed(evt);
            }
        });

        buttonEditar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/edit.png"))); // NOI18N
        buttonEditar.setToolTipText("Editar registro");
        buttonEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonEditarActionPerformed(evt);
            }
        });

        buttonBorrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/delete.png"))); // NOI18N
        buttonBorrar.setToolTipText("Borrar registro");
        buttonBorrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonBorrarActionPerformed(evt);
            }
        });

        buttonNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/add.png"))); // NOI18N
        buttonNuevo.setToolTipText("Crear un nuevo registro");
        buttonNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonNuevoActionPerformed(evt);
            }
        });

        tablaClientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaClientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaClientesMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tablaClientes);

        comboID.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione tipo de ID", "V", "E", "J" }));
        comboID.setEnabled(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addGap(10, 10, 10))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(19, 19, 19)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(labelCodigo)
                                    .addComponent(labelNombre)
                                    .addComponent(labelApellido))
                                .addGap(16, 16, 16))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(labelTelefono)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtApellidos, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtNombres, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labelDireccion, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(labelID, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(comboID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(buttonNuevo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonEditar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonGuardar1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonCancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonBorrar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(buttonBuscar)))
                .addGap(9, 9, 9))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(labelCodigo)
                            .addComponent(labelDireccion)
                            .addComponent(txtCodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(labelNombre)
                            .addComponent(txtNombres, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(labelApellido)
                            .addComponent(txtApellidos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelTelefono)
                    .addComponent(txtTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelID)
                    .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonNuevo)
                    .addComponent(buttonEditar)
                    .addComponent(buttonBorrar)
                    .addComponent(buttonGuardar1)
                    .addComponent(buttonCancelar)
                    .addComponent(buttonBuscar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 148, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void buttonBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonBuscarActionPerformed
        //Variable que guarda la palabra clave a buscar
        String busqueda = JOptionPane.showInputDialog(rootPane, "Ingrese palabra a buscar", "Busqueda", -1);
        reiniciarLista();
        //Variable auxiliar que guardara la lista de clientes filtrada
        ArrayList<Cliente> aux = new ArrayList<>();
        if (busqueda != null) {
            //Transforma lo ingresado todo a minuscula para evitar errores
            busqueda = busqueda.toLowerCase();
            for (final Cliente i : this.listaClientes) {
                if (Integer.toString(i.getCodigoCliente()).contains(busqueda)
                        || i.getNombre().toLowerCase().contains(busqueda)
                        || i.getApellido().toLowerCase().contains(busqueda)
                        || i.getTelefono().contains(busqueda)
                        || i.getId().toLowerCase().contains(busqueda)) {
                    aux.add(i);
                }
            }

            if (aux.isEmpty()) {
                //Si es vacio muestra un mensaje de error
                JOptionPane.showMessageDialog(rootPane, "No se encontraron resultados");
            } else {
                //Si obtiene algo lo guarda con la nueva lista y vuelve a llenar la tabla
                this.listaClientes = aux;
                generarVista(0);
                llenarTabla();
            }
        }

    }//GEN-LAST:event_buttonBuscarActionPerformed

    private void buttonGuardar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonGuardar1ActionPerformed
        //Variable que guarda el error de la Base de Datos
        boolean sqlError;
        String error;
        //Validacion directa
        error = validator();

        if (error == null) {
            //Si controller es igual a 1 crea un nuevo registro en la tabla CLIENTES
            if (controller == 1) {
                Cliente newClient
                        = new Cliente(0, txtNombres.getText(), txtApellidos.getText(),
                                txtTelefono.getText(), txtDireccion.getText(),
                                getIDFromCombo());

                sqlError = misDatos.createNewClient(newClient);

                if (sqlError) {
                    JOptionPane.showMessageDialog(rootPane, "El Cliente "
                            + txtNombres.getText() + " " + txtApellidos.getText()
                            + " fue agregado exitosamente");

                    finishActionButton();
                    disableFields();
                    reiniciarLista();

                } else {
                    JOptionPane.showMessageDialog(rootPane, "Hubo un error al ingresar"
                            + " el cliente, por favor notifique al analista de IT");
                }
                //Si controller no es igual a 1 edita un registro en la tabla CLIENTES
            } else {
                Cliente updateClient
                        = new Cliente(0, txtNombres.getText(), txtApellidos.getText(),
                                txtTelefono.getText(), txtDireccion.getText(),
                                getIDFromCombo());

                sqlError = misDatos.updateClient(updateClient, this.updateCodigo);

                if (sqlError) {
                    JOptionPane.showMessageDialog(rootPane, "El Cliente "
                            + txtNombres.getText() + " " + txtApellidos.getText()
                            + " fue actualizado exitosamente");

                    finishActionButton();
                    disableFields();
                    reiniciarLista();

                } else {
                    JOptionPane.showMessageDialog(rootPane, "Hubo un error al actualizar"
                            + " el cliente, por favor notifique al analista de IT");
                }
            }

        } else {
            JOptionPane.showMessageDialog(rootPane, error, "Error al ingresar", 0);
        }
    }//GEN-LAST:event_buttonGuardar1ActionPerformed

    private void buttonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonCancelarActionPerformed
        finishActionButton();
        disableFields();
        reiniciarLista();
    }//GEN-LAST:event_buttonCancelarActionPerformed

    private void buttonEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonEditarActionPerformed
        beginActionButton();
        enableFields();
        this.controller = 2;
        updateCodigo = Integer.parseInt(txtCodigo.getText());
    }//GEN-LAST:event_buttonEditarActionPerformed

    private void buttonBorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonBorrarActionPerformed

        int confirmacion = JOptionPane.showConfirmDialog(rootPane,
                "¿Esta seguro que desea eliminar el cliente "
                + vistaCliente.getNombre() + " " + vistaCliente.getApellido() + " ?");

        boolean sqlError;

        if (confirmacion == 0) {
            sqlError = misDatos.deleteClient(vistaCliente.getCodigoCliente());
            if (sqlError) {
                JOptionPane.showMessageDialog(rootPane, "El cliente "
                        + vistaCliente.getNombre() + " " + vistaCliente.getApellido()
                        + " fue eliminado exitosamente");
                reiniciarLista();
            } else {
                JOptionPane.showMessageDialog(rootPane, "Hubo un error al eliminar"
                        + " el cliente, por favor notifique al analista de IT");
            }
        }
    }//GEN-LAST:event_buttonBorrarActionPerformed

    private void buttonNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonNuevoActionPerformed
        beginActionButton();
        enableFields();
        cleanFields();
        this.controller = 1;
    }//GEN-LAST:event_buttonNuevoActionPerformed

    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened
        reiniciarLista();
    }//GEN-LAST:event_formInternalFrameOpened

    private void tablaClientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaClientesMouseClicked
        this.cursor = tablaClientes.getSelectedRow();
        generarVista(this.cursor);
    }//GEN-LAST:event_tablaClientesMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonBorrar;
    private javax.swing.JButton buttonBuscar;
    private javax.swing.JButton buttonCancelar;
    private javax.swing.JButton buttonEditar;
    private javax.swing.JButton buttonGuardar1;
    private javax.swing.JButton buttonNuevo;
    private javax.swing.JComboBox<String> comboID;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel labelApellido;
    private javax.swing.JLabel labelCodigo;
    private javax.swing.JLabel labelDireccion;
    private javax.swing.JLabel labelID;
    private javax.swing.JLabel labelNombre;
    private javax.swing.JLabel labelTelefono;
    private javax.swing.JTable tablaClientes;
    private javax.swing.JTextField txtApellidos;
    private javax.swing.JTextField txtCodigo;
    private javax.swing.JTextArea txtDireccion;
    private javax.swing.JTextField txtID;
    private javax.swing.JTextField txtNombres;
    private javax.swing.JTextField txtTelefono;
    // End of variables declaration//GEN-END:variables
}
