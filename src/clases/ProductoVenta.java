
package clases;

 //Esta clase hace un modelo de la tabla PRODUCTOS_VENTAS de la base de datos
public class ProductoVenta {
    
    private int codigoVenta;
    private int codigoProducto;
    private String nombreProducto;
    private int cantidad;

    public ProductoVenta() {
    }

    public ProductoVenta(int codigoVenta, int codigoProducto, String nombreProducto, int cantidad) {
        this.codigoVenta = codigoVenta;
        this.codigoProducto = codigoProducto;
        this.nombreProducto = nombreProducto;
        this.cantidad = cantidad;
    }

    public int getCodigoVenta() {
        return codigoVenta;
    }

    public void setCodigoVenta(int codigoVenta) {
        this.codigoVenta = codigoVenta;
    }

    public int getCodigoProducto() {
        return codigoProducto;
    }

    public void setCodigoProducto(int codigoProducto) {
        this.codigoProducto = codigoProducto;
    }

    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    
    
}
