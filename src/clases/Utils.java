package clases;

import java.util.Calendar;
import java.util.Date;

//Esta clase se utiliza para depositar metodos estaticos que ayudan en momentos puntuales en el sistema
public class Utils {
    
    //Este metodo devuelve la fecha actual de el sistema como un dato tipo Date
    public static Date getDate(){
        //Se utiliza la clase Calendar para llamar la fecha actual de el sistema
        return Calendar.getInstance().getTime();
    }
    
    //Este metodo recibe una fecha tipo Date y lo devuelve como un String en formato dd/mm/yyyy
    public static String printDate(Date date){
        //Variable que contendra la fecha en formato dd/mm/yyyy para devolverla
        String aux = null;
        
        //Se verifica primero que la fecha que se paso como parametro no sea null
        if (date != null){
            
            //Se utiliza el metodo valueOf de la clase String para transformar las variables tipo Date 
            aux = String.valueOf(date.getDate()) + "/" + String.valueOf(date.getMonth())
                    + "/" + String.valueOf(date.getYear()+1900);
            
        }
        
        //Si date no es null, aux devuelve la fecha en una variable tipo String en formato dd/mm/yyyy
        return aux;
    }
    
    //Este metodo se utiliza para transformar los datos Date de la libreria java.util a java.sql
    public static java.sql.Date convertUtilToSql(java.util.Date uDate) {

        java.sql.Date sDate = new java.sql.Date(uDate.getTime());

        return sDate;

    }
    
    //Este metodo se utiliza para transformar los datos Date de la libreria java.sql a java.util
    public static java.util.Date convertSqlToUtil(java.sql.Date sDate){
        
        java.util.Date uDate = new java.util.Date(sDate.getTime());
        
        return uDate;
    }

}
