package clases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Datos {
    //Variable para la conexion a la base de datos
    private Connection cnn;
    //Variable con la lista de usuarios disponible  
    private ArrayList<Usuario> listaUsuarios = new ArrayList<>();
    //Variable que guarda el usuario de sesion durante el programa
    private Usuario sessionUser = new Usuario();

    public Datos() {
        // Se establece la conexion a la base de datos mySql
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String db = "jdbc:mysql://localhost/proyectodb";
            cnn = DriverManager.getConnection(db, "root", "");
        } catch (Exception ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    //Este metodo devuelve true si consigue algun usuario y contraseña que los parametros de entrada
    public boolean loginValidator(String user, String password) {

        try {
            String sql = "SELECT * FROM USUARIOS WHERE ID_USUARIO = '" + user + "'"
                    + " AND CLAVE = '" + password + "'";
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            if (rs.next()) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }
    
    //Este metodo cambia el usuario de sesion para la opcion de cambio de usuario
    public void setSessionUser(String user){
        
         try {
            String sql = "SELECT * FROM USUARIOS WHERE ID_USUARIO = '" + user + "'";
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                
                this.sessionUser.setIdUsuario(rs.getString("ID_USUARIO"));
                this.sessionUser.setNombre(rs.getString("NOMBRE"));
                this.sessionUser.setApellido(rs.getString("APELLIDO"));
                this.sessionUser.setClave(rs.getString("CLAVE"));
                this.sessionUser.setPerfil(rs.getInt("PERFIL"));
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    //Este metodo genera una lista de usuarios y la guarda en la vaiable local
    public void generarListaUsuarios() {

        ArrayList<Usuario> list = new ArrayList<>();
        Usuario temp;

        try {

            String sql = "SELECT * FROM USUARIOS";
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                temp = new Usuario();
                temp.setIdUsuario(rs.getString("ID_USUARIO"));
                temp.setNombre(rs.getString("NOMBRE"));
                temp.setApellido(rs.getString("APELLIDO"));
                temp.setClave(rs.getString("CLAVE"));
                temp.setPerfil(rs.getInt("PERFIL"));
                list.add(temp);

            }
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }

        this.listaUsuarios = list;
    }
    
    //Este metodo se utiliza para no repetir ID de usuario cuando se crea uno nuevo
    //retorna 1 cuando ya existe el ID de lo contrario retorna 0
    public int validacionIdUsuario(String id) {
        try {
            String sql = "SELECT * FROM USUARIOS WHERE ID_USUARIO = '"
                    + id + "'";
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            if (rs.next()) {
                return 1;
            } else {
                return 0;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }

    }

    //Este metodo crea un usuario nuevo con un ID usuario como clave primaria
    public boolean createNewUser(Usuario user) {
        try {
            String sql = "INSERT INTO USUARIOS "
                    + "(ID_USUARIO, NOMBRE, APELLIDO, CLAVE, PERFIL) VALUES (?,?,?,?,?)";
            PreparedStatement st = cnn.prepareStatement(sql);
            st.setString(1, user.getIdUsuario());
            st.setString(2, user.getNombre());
            st.setString(3, user.getApellido());
            st.setString(4, user.getClave());
            st.setInt(5, user.getPerfil());

            st.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    //Este metodo te permite editar un Usuario buscandolo por el ID_USUARIO
    public boolean updateUser(Usuario user, String id) {
        try {
            String sql = "UPDATE USUARIOS SET ID_USUARIO = ?, NOMBRE = ?, APELLIDO = ?,"
                    + "CLAVE = ?, PERFIL = ? WHERE ID_USUARIO = ?";
            PreparedStatement st = cnn.prepareStatement(sql);
            st.setString(1, user.getIdUsuario());
            st.setString(2, user.getNombre());
            st.setString(3, user.getApellido());
            st.setString(4, user.getClave());
            st.setInt(5, user.getPerfil());
            st.setString(6, id);

            st.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    //Este metodo se utiliza para eliminar un usuario buscandolo por su ID_USUARIO
    public boolean deleteUser(String idUser) {
        try {
            String sql = "DELETE FROM USUARIOS WHERE ID_USUARIO = '" + idUser + "'";
            Statement st = cnn.createStatement();
            st.executeUpdate(sql);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }

    //Este metodo retorna una lista con todos los registros de la tabla CLIENTES
    public ArrayList<Cliente> generarListaClientes() {
        ArrayList<Cliente> listaClientes = new ArrayList<>();
        Cliente temp;

        try {
            String sql = "SELECT * FROM CLIENTES";
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                temp = new Cliente();
                temp.setCodigoCliente(rs.getInt("CODIGO_CLIENTE"));
                temp.setNombre(rs.getString("NOMBRE"));
                temp.setApellido(rs.getString("APELLIDO"));
                temp.setTelefono(rs.getString("TELEFONO"));
                temp.setDireccion(rs.getString("DIRECCION"));
                temp.setId(rs.getString("ID"));
                listaClientes.add(temp);
            }
            
            
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return listaClientes;
    }
    
    //Este metodo permite crear un registro en la tabla CLIENTES
    public boolean createNewClient(Cliente client) {
        try {
            String sql = "INSERT INTO CLIENTES "
                    + "(NOMBRE, APELLIDO, TELEFONO, DIRECCION, ID) VALUES (?,?,?,?,?)";
            PreparedStatement st = cnn.prepareStatement(sql);
            st.setString(1, client.getNombre());
            st.setString(2, client.getApellido());
            st.setString(3, client.getTelefono());
            st.setString(4, client.getDireccion());
            st.setString(5, client.getId());

            st.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }
    
    //Este metodo se utiliza para editar un cliente de la tabla CLIENTES buscandolo
    //por su CODIGO_CLIENTE
    public boolean updateClient(Cliente client, int updateCode) {
        try {
            String sql = "UPDATE CLIENTES SET NOMBRE = ?, APELLIDO = ?,"
                    + "TELEFONO = ?, DIRECCION = ?, ID = ? WHERE CODIGO_CLIENTE = ?";
            PreparedStatement st = cnn.prepareStatement(sql);
            st.setString(1, client.getNombre());
            st.setString(2, client.getApellido());
            st.setString(3, client.getTelefono());
            st.setString(4, client.getDireccion());
            st.setString(5, client.getId());
            st.setInt(6, updateCode);

            st.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }
    
    //Este metodo permite eliminar un registro de la tabla CLIENTES buscandolo
    //por el parametro CODIGO_CLIENTE
    public boolean deleteClient(int clientCode) {
        try {
            String sql = "DELETE FROM CLIENTES WHERE CODIGO_CLIENTE = " + clientCode;
            Statement st = cnn.createStatement();
            st.executeUpdate(sql);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }
    
    //Este metodo retorna una lista con todos los registros de la tabla PRODUCTOS
    public ArrayList<Producto> generarListaProductos() {
        ArrayList<Producto> listaProductos = new ArrayList<>();
        Producto temp;

        try {
            String sql = "SELECT * FROM PRODUCTOS";
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                temp = new Producto();
                temp.setCodigoProducto(rs.getInt("CODIGO_PRODUCTO"));
                temp.setNombreProducto(rs.getString("NOMBRE_PRODUCTO"));
                temp.setDescripcion(rs.getString("DESCRIPCION"));
                temp.setMarca(rs.getString("MARCA"));
                temp.setCantidad(rs.getInt("CANTIDAD"));
                temp.setPrecio(rs.getFloat("PRECIO"));
                listaProductos.add(temp);
            }
            
            
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return listaProductos;
    }
    //Este metodo retorna una lista con todos los registros de la tabla PRODUCTOS
    //que tienen un valor en CANTIDAD mayor a 0
    public ArrayList<Producto> generarListaProductosDisponibles() {
        ArrayList<Producto> listaProductos = new ArrayList<>();
        Producto temp;

        try {
            String sql = "SELECT * FROM PRODUCTOS WHERE CANTIDAD > 0";
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                temp = new Producto();
                temp.setCodigoProducto(rs.getInt("CODIGO_PRODUCTO"));
                temp.setNombreProducto(rs.getString("NOMBRE_PRODUCTO"));
                temp.setDescripcion(rs.getString("DESCRIPCION"));
                temp.setMarca(rs.getString("MARCA"));
                temp.setCantidad(rs.getInt("CANTIDAD"));
                temp.setPrecio(rs.getFloat("PRECIO"));
                listaProductos.add(temp);
            }
            
            
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return listaProductos;
    }
    
    //Este metodo te permite crear un nuevo registro en la tabla PRODUCTOS
    public boolean createNewProduct(Producto product) {
        try {
            String sql = "INSERT INTO PRODUCTOS "
                    + "(NOMBRE_PRODUCTO, DESCRIPCION, MARCA, CANTIDAD, PRECIO) VALUES (?,?,?,?,?)";
            PreparedStatement st = cnn.prepareStatement(sql);
            st.setString(1, product.getNombreProducto());
            st.setString(2, product.getDescripcion());
            st.setString(3, product.getMarca());
            st.setInt(4, product.getCantidad());
            st.setFloat(5, product.getPrecio());

            st.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }
    
    //Este metodo te permite editar un registro de la tabla PRODUCTOS ubicado por el
    //CODIGO_PRODUCTO
    public boolean updateProduct(Producto product, int updateCode) {
        try {
            String sql = "UPDATE PRODUCTOS SET NOMBRE_PRODUCTO = ?, DESCRIPCION = ?,"
                    + "MARCA = ?, CANTIDAD = ?, PRECIO = ? WHERE CODIGO_PRODUCTO = ?";
            PreparedStatement st = cnn.prepareStatement(sql);
            st.setString(1, product.getNombreProducto());
            st.setString(2, product.getDescripcion());
            st.setString(3, product.getMarca());
            st.setInt(4, product.getCantidad());
            st.setFloat(5, product.getPrecio());
            st.setInt(6, updateCode);

            st.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }
    
    //Este metodo te permite eliminar un registro de la tabla PRODUCTOS
    public boolean deleteProduct(int productCode) {
        try {
            String sql = "DELETE FROM PRODUCTOS WHERE CODIGO_PRODUCTO = " + productCode;
            Statement st = cnn.createStatement();
            st.executeUpdate(sql);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }
    
    //Este metodo te permite cambiar el atributo CANTIDAD del registro de la tabla 
    //como parametros el CODIGO_PRODUCTO y la cantidad que se le restara en el atributo CANTIDAD
    public boolean subtractProduct(int codigoProducto,int cantSubtract){
        int cantidad = 0;
        //Se consulta el valor del atributo CANTIDAD por el CODIGO_PRODUCTO
         try {
            String sql = "SELECT * FROM PRODUCTOS WHERE CODIGO_PRODUCTO = " + codigoProducto;
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                cantidad = rs.getInt("CANTIDAD");
            }
            
            
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
         //Se resta el valor obtenido por el ingresado como parametro
         cantidad = cantidad - cantSubtract;
        //Se edita el valor CANTIDAD del registro de la tabla PRODUCTOS
        try {
            String sql = "UPDATE PRODUCTOS SET  CANTIDAD = ? "
                    + "WHERE CODIGO_PRODUCTO = ?";
            PreparedStatement st2 = cnn.prepareStatement(sql);
            st2.setInt(1, cantidad);
            st2.setInt(2, codigoProducto);

            st2.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
    }
    
    //Este metodo te permite cambiar la contraseña del usuario
    public boolean changePassword(String idUser, String newPass){
        try {
            String sql = "UPDATE USUARIOS SET CLAVE = ? WHERE ID_USUARIO = ?";
            PreparedStatement st = cnn.prepareStatement(sql);
            st.setString(1, newPass);
            
            st.setString(2, idUser);

            st.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    //Este metodo retorna una lista con todos los registros de la tabla VENTAS
    public ArrayList<Venta> generarListaVentas(){
        
        ArrayList<Venta> listaVentas = new ArrayList<>();
        Venta temp;

        try {
            String sql = "SELECT * FROM VENTAS";
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                temp = new Venta();
                temp.setCodigoVenta(rs.getInt("CODIGO_VENTA"));
                temp.setCodigoCliente(rs.getInt("CODIGO_CLIENTE"));
                temp.setPagoTotal(rs.getFloat("PAGO_TOTAL"));
                temp.setModoDePago(rs.getString("MODO_DE_PAGO"));
                temp.setTipoEntrega(rs.getString("TIPO_ENTREGA"));
                temp.setFecha(rs.getDate("FECHA"));
                listaVentas.add(temp);
            }
            
            
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return listaVentas;
        
    }
    
    //Este metodo permite crear un nuevo registro en la tabla VENTAS
    public boolean createNewVenta(Venta venta) {
        try {
            String sql = "INSERT INTO VENTAS "
                    + "(CODIGO_CLIENTE, PAGO_TOTAL, MODO_DE_PAGO, TIPO_ENTREGA, FECHA) VALUES (?,?,?,?,?)";
            PreparedStatement st = cnn.prepareStatement(sql);
            st.setInt(1, venta.getCodigoCliente());
            st.setFloat(2, venta.getPagoTotal());
            st.setString(3, venta.getModoDePago());
            st.setString(4, venta.getTipoEntrega());
            st.setDate(5, Utils.convertUtilToSql( venta.getFecha()));

            st.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }
    
    //Este metodo permite eliminar un registro de la tabla VENTAS
    public boolean deleteVenta(int ventaCode) {
        try {
            String sql = "DELETE FROM VENTAS WHERE CODIGO_VENTA = " + ventaCode;
            Statement st = cnn.createStatement();
            st.executeUpdate(sql);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }
    
    //Este metodo permite crear un registro en la tabla PRODUCTOS_VENTAS
    public boolean createNewProductoVenta(ProductoVenta productoVenta) {
        try {
            String sql = "INSERT INTO PRODUCTOS_VENTAS "
                    + "(CODIGO_PRODUCTO, CODIGO_VENTA, NOMBRE_PRODUCTO, CANTIDAD) VALUES (?,?,?,?)";
            PreparedStatement st = cnn.prepareStatement(sql);
            st.setInt(1, productoVenta.getCodigoProducto());
            st.setInt(2, productoVenta.getCodigoVenta());
            st.setString(3, productoVenta.getNombreProducto());
            st.setInt(4, productoVenta.getCantidad());

            st.executeUpdate();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }
    
    //Este metodo permite eliminar un registro de la tabla PRODUCTOS_VENTAS
    public boolean deleteProductoVenta(int productoVentaCode) {
        try {
            String sql = "DELETE FROM PRODUCTOS_VENTAS WHERE CODIGO_VENTA = " + productoVentaCode;
            Statement st = cnn.createStatement();
            st.executeUpdate(sql);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

    }
    
    //Este metodo retorna una lista con todos los registros de la tabla PPRODUCTOS_VENTAS
    public ArrayList<ProductoVenta> generarListaProductosVentas(int codigoVenta){
        
        ArrayList<ProductoVenta> listaProductoVenta = new ArrayList<>();
        ProductoVenta temp;

        try {
            String sql = "SELECT * FROM PRODUCTOS_VENTAS WHERE CODIGO_VENTA =" + codigoVenta;
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                temp = new ProductoVenta();
                temp.setCodigoVenta(rs.getInt("CODIGO_VENTA"));
                temp.setCodigoProducto(rs.getInt("CODIGO_PRODUCTO"));
                temp.setCantidad(rs.getInt("CANTIDAD"));
                temp.setNombreProducto(rs.getString("NOMBRE_PRODUCTO"));
                listaProductoVenta.add(temp);
            }
            
            
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return listaProductoVenta;
        
    }
    
    //Este metodo retorna el CODIGO_VENTA del ultimo registro
    public int getLastCodigoVentaFromVentas() {

        int codigo = -1;
        try {

            String sql = "SELECT * FROM VENTAS WHERE CODIGO_VENTA = (SELECT MAX(CODIGO_VENTA) FROM VENTAS)";
            Statement st = cnn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                
                codigo = rs.getInt("CODIGO_VENTA");

            }
        } catch (SQLException ex) {
            Logger.getLogger(Datos.class.getName()).log(Level.SEVERE, null, ex);
        }
        return codigo;
    }

    //Getters and Setters
    public Usuario getSessionUser() {
        return sessionUser;
    }

    public void setSessionUser(Usuario sessionUser) {
        this.sessionUser = sessionUser;
    }

    public ArrayList<Usuario> getListaUsuarios() {
        return listaUsuarios;
    }

    public void setListaUsuarios(ArrayList<Usuario> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

}
