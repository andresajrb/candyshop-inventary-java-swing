
package clases;

import formularios.frmLogin;

public class SistemaProyecto {

   
    public static void main(String[] args) {
        //Genero la clase de datos
        Datos misDatos = new Datos();
        
        //Genero las ventanas
        frmLogin miLogin = new frmLogin();
        miLogin.setDatos(misDatos);
        miLogin.setLocationRelativeTo(null);
        miLogin.setVisible(true);
        
    }
    
}
