
package clases;

import java.util.Date;

 //Esta clase hace un modelo de la tabla VENTAS de la base de datos
public class Venta {
    
    private int codigoVenta;
    private int codigoCliente;
    private float pagoTotal;
    private String modoDePago;
    private String tipoEntrega;
    private Date fecha;

    public Venta() {
    }

    public Venta(int codigoVenta, int codigoCliente, float pagoTotal, String modoDePago, String tipoEntrega, Date fecha) {
        this.codigoVenta = codigoVenta;
        this.codigoCliente = codigoCliente;
        this.pagoTotal = pagoTotal;
        this.modoDePago = modoDePago;
        this.tipoEntrega = tipoEntrega;
        this.fecha = fecha;
    }

    public int getCodigoVenta() {
        return codigoVenta;
    }

    public void setCodigoVenta(int codigoVenta) {
        this.codigoVenta = codigoVenta;
    }

    public int getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(int codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public float getPagoTotal() {
        return pagoTotal;
    }

    public void setPagoTotal(float pagoTotal) {
        this.pagoTotal = pagoTotal;
    }

    public String getModoDePago() {
        return modoDePago;
    }

    public void setModoDePago(String modoDePago) {
        this.modoDePago = modoDePago;
    }

    public String getTipoEntrega() {
        return tipoEntrega;
    }

    public void setTipoEntrega(String tipoEntrega) {
        this.tipoEntrega = tipoEntrega;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }
    
    
}
