-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-09-2018 a las 23:02:33
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyectodb`
--
CREATE DATABASE IF NOT EXISTS `proyectodb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `proyectodb`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `CODIGO_CLIENTE` int(3) NOT NULL,
  `NOMBRE` varchar(15) DEFAULT NULL,
  `APELLIDO` varchar(15) DEFAULT NULL,
  `TELEFONO` varchar(13) DEFAULT NULL,
  `DIRECCION` text,
  `ID` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='CLIENTES REGISTRADOS';

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`CODIGO_CLIENTE`, `NOMBRE`, `APELLIDO`, `TELEFONO`, `DIRECCION`, `ID`) VALUES
(1, 'Pedro', 'Perez', '04143254520', 'San Diego, Yuma 1', 'V20365987'),
(2, 'Santiago', 'Cardozo', '04165489562', 'San Joaquin', 'V16598785'),
(3, 'Ernesto', 'Valverde', '04123658945', 'Av. Madrid, Bucaral', 'E12356987'),
(4, 'Rodrigo', 'Figuera', '04165698787', 'Campo Solo, San Diego, Carabobo', 'V13569874'),
(5, 'Gregor', 'Blanco', '04142569898', 'Av. Michelena', 'V15895985'),
(6, 'Elian', 'Noguera', '04242425656', 'San Joaquin, Carabobo', 'V18956956'),
(7, 'Axel', 'Hurtado', '04123256565', 'Flor Amarillo, Valencia', 'V16236236'),
(8, 'Elias', 'Beltran', '041412312', 'Portal 2 , Flor Amarillo, Carabobo', 'V20203365'),
(9, 'Julen', 'Lopetegui', '02412562525', 'Av. Madrid, Reemplazo', 'E9656632'),
(10, 'Mara', 'Salva', '04122356565', 'Nuevo Salvador, Av. Diego Ibarra, Carabobo', 'V14156156'),
(11, 'Lupita', 'Rodriguez', '04142562569', 'Av Eloy Blanco', 'V15123659');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `CODIGO_PRODUCTO` int(3) NOT NULL,
  `NOMBRE_PRODUCTO` varchar(30) NOT NULL,
  `DESCRIPCION` varchar(50) DEFAULT NULL,
  `MARCA` varchar(15) DEFAULT NULL,
  `CANTIDAD` int(3) NOT NULL,
  `PRECIO` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='TABLA DE PRODUCTOS';

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`CODIGO_PRODUCTO`, `NOMBRE_PRODUCTO`, `DESCRIPCION`, `MARCA`, `CANTIDAD`, `PRECIO`) VALUES
(4, 'Atun Enlatado', '170 gr.', '', 30, 71),
(5, 'Carton de huevo', '30 unidades', '', 15, 81.5),
(6, 'Mortadela', '1 kilogramo', '', 20, 18),
(7, 'Leche Pasteurizada', '1 litro', '', 18, 48.5),
(8, 'Mantequilla', '500 gramos', '', 14, 60),
(9, 'Sardina en lata', '170 gramos', '', 30, 24),
(10, 'Arveja', '1 kilogramo', '', 25, 65),
(11, 'Lenteja', '1 kilogramo', '', 25, 53),
(12, 'Caraota', '1 kilogramo', '', 25, 72),
(13, 'Frijol', '1 kilogramo', '', 25, 24),
(14, 'Aceite comestible', '1 litro', '', 30, 50),
(15, 'Azucar refinada', '1 kilogramo', '', 25, 40),
(16, 'Cafe molido', '500 gramos', '', 20, 70),
(17, 'Mayonesa', '500 gramos', '', 25, 35),
(18, 'Margarina', '500 gramos', '', 15, 39.5),
(19, 'Pasta', '1 kilogramo', '', 30, 48),
(20, 'Sal de mesa', '1 kilogramo', '', 15, 15);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos_ventas`
--

CREATE TABLE `productos_ventas` (
  `CODIGO_PRODUCTO` int(4) NOT NULL,
  `CODIGO_VENTA` int(4) NOT NULL,
  `NOMBRE_PRODUCTO` varchar(20) DEFAULT NULL,
  `CANTIDAD` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `ID_USUARIO` varchar(15) NOT NULL,
  `NOMBRE` varchar(15) DEFAULT NULL,
  `APELLIDO` varchar(15) DEFAULT NULL,
  `CLAVE` varchar(10) NOT NULL,
  `PERFIL` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='USUARIOS QUE PUEDEN INGRESAR AL SISTEMA';

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`ID_USUARIO`, `NOMBRE`, `APELLIDO`, `CLAVE`, `PERFIL`) VALUES
('admin1', 'Administrador', '', '123', 1),
('worker1', 'Empleado', '', '123', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `CODIGO_VENTA` int(6) NOT NULL,
  `CODIGO_CLIENTE` int(3) NOT NULL,
  `PAGO_TOTAL` float NOT NULL,
  `MODO_DE_PAGO` varchar(20) DEFAULT NULL,
  `TIPO_ENTREGA` varchar(20) DEFAULT NULL,
  `FECHA` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='VENTAS REGISTRADAS';

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`CODIGO_CLIENTE`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`CODIGO_PRODUCTO`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`ID_USUARIO`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`CODIGO_VENTA`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `CODIGO_CLIENTE` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `CODIGO_PRODUCTO` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `CODIGO_VENTA` int(6) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
